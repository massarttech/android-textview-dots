# Android Dots TextView

Android Library to show dots Animation on a android textview to show loading type of thing 

![](https://gitlab.com/massarttech/android-textview-dots/blob/master/app/src/main/res/raw/loading.gif?inline=false)

### To get a Library into your build:

Step 1. Add the JitPack repository to your build file 

Add it in your root build.gradle at the end of repositories:

```allprojects { 
repositories {
...
maven { url 'https://jitpack.io' }
}
}
```
  
Step 2. Add the dependency in your app level gradle
  
```
dependencies { 
	implementation 'com.gitlab.massarttech:android-textview-dots:1.0.2'
}
```
  
### Features
 
 * Add the count of dots you want to display to the user
 * Add text to show on left of animated dots like loading...
 * Can also show only dots with animation
 
 ### Class and Methods
 
 In your `.xml` call view 
 
 ```
 <com.massarttech.android.dots.textview.TextAndAnimationView
        xmlns:app="http://schemas.android.com/apk/res-auto"
        android:id="@+id/animated_dots"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:numberOfDots="10"
        app:setText="Loading"
        app:setTextSize="30" />
```
 
`app:numberOfDots` - No of dots to show on screen
`app:setText` - Show text with dots
`app:setTextSize` - Decides Text size
 
 In `.java` or `.kt` don't forget to call stopAnimation():
 
```
override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textAndAnimationView = findViewById(R.id.animated_dots)
    }

override fun onStop() {
        super.onStop()
        textAndAnimationView.stopAnimation()
    }
```

### Credits
https://github.com/rajputkapil/textviewdotsanimation
  
