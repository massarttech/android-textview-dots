package com.massarttech.android.dots.textview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.massarttech.android.dots.textview.databinding.DotsLayoutBinding


class TextAndAnimationView : LinearLayout {

    private lateinit var binding: DotsLayoutBinding

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        showTextAndAnimation(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        showTextAndAnimation(context, attrs)
    }

    /**
     * Call this method to start showing animation of dots
     * @param context: activity Context to pass
     * @param attrs: attribute set
     *
     */
    private fun showTextAndAnimation(context: Context, attrs: AttributeSet) {

        binding = DotsLayoutBinding.inflate(LayoutInflater.from(context), this, true)

        val ta = context.obtainStyledAttributes(attrs, R.styleable.TextAndAnimationView, 0, 0)
        try {
            val text = ta.getText(R.styleable.TextAndAnimationView_setText)
            val textHint = ta.getText(R.styleable.TextAndAnimationView_setTextHint)
            val color = ta.getInt(R.styleable.TextAndAnimationView_setTextColor, 0)
            val textSize = ta.getFloat(R.styleable.TextAndAnimationView_setTextSize, 0f)
            val dotsCount = ta.getInt(R.styleable.TextAndAnimationView_numberOfDots, 0)
            if (text != null)
                setText(text)
            if (textHint != null)
                setTextHint(textHint)
            if (color != 0)
                setTextColor(color)
            if (textSize != 0f)
                setTextSize(textSize)
            if (dotsCount != 0)
                noOfDots(dotsCount)
        } finally {
            ta.recycle()
        }

        binding.dotsProgressDotsTxt.showDotsAnimation()
    }

    /**
     *
     */
    /**
     *Shows text like loading
     */
    fun setText(text: CharSequence) {
        binding.dotsTextToShow.setText(text)
    }

    /**
     *text size
     */
    fun setTextSize(size: Float) {
        binding.dotsTextToShow.setTextSize(size)
        binding.dotsProgressDotsTxt.setTextSize(size)
    }

    /**
     *text hint
     */
    fun setTextHint(textHint: CharSequence) {
        binding.dotsTextToShow.setHint(textHint)
    }

    /**
     *text color
     */
    fun setTextColor(color: Int) {
        binding.dotsTextToShow.setTextColor(color)
        binding.dotsProgressDotsTxt.setTextColor(color)
    }

    /**
     *Remember to call this when activity stops, to stop handler
     */
    fun stopAnimation() {
        binding.dotsProgressDotsTxt.stopAnimation()
    }

    fun startAnimation() {
        binding.dotsProgressDotsTxt.showDotsAnimation()
    }

    /**
     *Animated dots count
     */
    fun noOfDots(dotsCount: Int) {
        binding.dotsProgressDotsTxt.noOfDots(dotsCount)
    }

    /**
     *delay in animation dots, define in ms
     */
    fun animationDelay(animationDelayTime: Long) {
        binding.dotsProgressDotsTxt.animationDelay(animationDelayTime)
    }
}
