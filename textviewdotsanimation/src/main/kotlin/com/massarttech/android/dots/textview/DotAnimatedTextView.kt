package com.massarttech.android.dots.textview

import android.content.Context
import android.graphics.Rect
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatTextView


open class DotAnimatedTextView : AppCompatTextView {

    private var threadHandler: Handler? = null

    private var runnable: Runnable? = null

    private var dotsCount: Int = 4 // default is 4 dots count

    private var animationDelayTime: Long = 500 // default is 5 ms

    private var tempDots: Int = 0

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    /**
     *call to start showing Animation
     */
    fun showDotsAnimation() {
        setWidthToRemoveAnimationLack()
        text = ""
        if (threadHandler == null && runnable == null) {
            threadHandler = Handler(Looper.getMainLooper())
            runnable = object : Runnable {
                override fun run() {

                    threadHandler?.postDelayed(this, animationDelayTime)
                    if (tempDots == dotsCount) {
                        tempDots = 0
                        text = ""
                    } else {
                        text = getDot(++tempDots)
                    }
                    invalidate()
                }
            }
            runnable?.run()
        }
    }

    /**
     *Simple hack to overcome layout vibration
     */
    fun setWidthToRemoveAnimationLack() {
        visibility = View.INVISIBLE
        text = getDot(dotsCount)
        val bounds = Rect()
        val textPaint = this.paint
        textPaint.getTextBounds(getDot(dotsCount), 0, this.length(), bounds)
        val width = bounds.width() + 100
        val params = LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT)
        this.layoutParams = params
        visibility = View.VISIBLE
    }

    /**
     *logic to set dots according to dots count
     */
    fun getDot(dotNo: Int): String {
        val sb = StringBuilder()
        for (i in 1..dotNo) {
            sb.append(".")
        }
        return sb.toString()
    }

    fun stopAnimation() {
        runnable?.let {
            threadHandler?.removeCallbacks(it)
        }
    }

    fun noOfDots(dotsCount: Int) {
        this.dotsCount = dotsCount
    }

    fun animationDelay(animationDelayTime: Long) {
        this.animationDelayTime = animationDelayTime
    }
}
