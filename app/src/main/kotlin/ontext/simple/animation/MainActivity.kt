package ontext.simple.animation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.massarttech.android.dots.textview.TextAndAnimationView


class MainActivity : AppCompatActivity() {

    private lateinit var textAndAnimationView: TextAndAnimationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textAndAnimationView = findViewById(R.id.animated_dots)
        //    textAndAnimationView.setText("Loading")
        //    textAndAnimationView.setTextSize(30f)
    }

    override fun onStop() {
        super.onStop()
        textAndAnimationView.stopAnimation()
    }
}
